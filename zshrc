export PATH=$HOME/bin:/usr/local/bin:$PATH:$HOME/go/bin:/opt/homebrew/bin/
export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="random"
ZSH_THEME_RANDOM_IGNORED=()
DISABLE_AUTO_UPDATE="true"
DISABLE_UPDATE_PROMPT="true"
plugins=(git osx wd tmux history extract
         zsh-autosuggestions)
source $ZSH/oh-my-zsh.sh
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS

export HOMEBREW_NO_AUTO_UPDATE=1
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# Local config
[[ -f ~/.zshrc.local ]] && source ~/.zshrc.local

# aliases
[[ -f ~/.aliases ]] && source ~/.aliases
