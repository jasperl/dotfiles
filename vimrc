syntax on
set showmode
set showcmd
set encoding=utf-8  
set expandtab
set softtabstop=2
set number
" set relativenumber
set cursorcolumn
set cursorline
set wrap
set scrolloff=5
set laststatus=2
set ruler
set showmatch
set hlsearch
set incsearch
set nobackup
set noerrorbells

